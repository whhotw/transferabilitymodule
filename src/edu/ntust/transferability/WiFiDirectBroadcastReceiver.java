package edu.ntust.transferability;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import edu.ntust.transferability.task.FileServerAsyncTask;

/**
 * Created by whh on 6/9/14.
 */
public class WiFiDirectBroadcastReceiver extends BroadcastReceiver
{

    private WifiP2pManager mManager;
    private WifiP2pManager.Channel mChannel;
    private ListViewListener listViewListener;
    private Context context;

    public List<WifiP2pDevice> peers = new ArrayList();

    public WiFiDirectBroadcastReceiver(WifiP2pManager manager, WifiP2pManager.Channel channel, ListViewListener listViewListener, Context context)
    {
        super();
        this.mManager = manager;
        this.mChannel = channel;
        this.listViewListener = listViewListener;
        this.context = context;
    }

    @Override
    public void onReceive(Context context, Intent intent)
    {
        String action = intent.getAction();

        if (WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION.equals(action))
        {
            // Check to see if Wi-Fi is enabled and notify appropriate activity
            int state = intent.getIntExtra(WifiP2pManager.EXTRA_WIFI_STATE, -1);
            if (state == WifiP2pManager.WIFI_P2P_STATE_ENABLED)
            {
                // Wifi P2P is enabled
                Log.d(ViewAppsActivity.tag, "!!! p2p enabled and support");
            }
            else
            {
                // Wi-Fi P2P is not enabled
                Log.d(ViewAppsActivity.tag, "!!! p2p not enabled and unsupported");
            }

        }
        else if (WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION.equals(action))
        {
            // Call WifiP2pManager.requestPeers() to get a list of current peers
            Log.d(ViewAppsActivity.tag, "!!! WIFI_P2P_PEERS_CHANGED_ACTION");
            if (mManager != null)
            {
                mManager.requestPeers(mChannel, peerListListener);
            }

        }
        else if (WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION.equals(action))
        {
            // Respond to new connection or disconnections
            Log.d(ViewAppsActivity.tag, "!!! WIFI_P2P_CONNECTION_CHANGED_ACTION");
            if (mManager == null)
            {
                return;
            }

            mManager.requestConnectionInfo(mChannel, connectionListener);

        }
        else if (WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION.equals(action))
        {
            // Respond to this device's wifi state changing
            Log.d(ViewAppsActivity.tag, "!!! WIFI_P2P_THIS_DEVICE_CHANGED_ACTION");
        }
    }

    public static interface ListViewListener
    {
        void updateListView();
    }

    WifiP2pManager.ConnectionInfoListener connectionListener = new WifiP2pManager.ConnectionInfoListener()
    {
        @Override
        public void onConnectionInfoAvailable(final WifiP2pInfo info)
        {
            // InetAddress from WifiP2pInfo struct.
            String groupOwnerAddress = null;
            if (info.groupOwnerAddress != null)
            {
                groupOwnerAddress = info.groupOwnerAddress.getHostAddress();
                Log.d(ViewAppsActivity.tag, "!!!" + groupOwnerAddress);
            }

            // After the group negotiation, we can determine the group owner.
            if (info.groupFormed && info.isGroupOwner)
            {
                // Do whatever tasks are specific to the group owner.
                // One common case is creating a server thread and accepting
                // incoming connections.
                FileServerAsyncTask fileServerAsyncTask = new FileServerAsyncTask();
                fileServerAsyncTask.execute(new Object[1]);
                Log.d(ViewAppsActivity.tag, "!!! create server");

            }
            else if (info.groupFormed)
            {
                // The other device acts as the client. In this case,
                // you'll want to create a client thread that connects to the group
                // owner.
                Log.d(ViewAppsActivity.tag, "!!! create client" + groupOwnerAddress);


                Thread thread = new Thread(new Runnable()
                {
                    int len;
                    byte buf[] = new byte[1024];

                    @Override
                    public void run()
                    {

                        try
                        {
                            Socket socket = new Socket();
                            socket.bind(null);
                            socket.connect(new InetSocketAddress(info.groupOwnerAddress.getHostAddress(), 12345), 1000);

                            /**
                             * Create a byte stream from a JPEG file and pipe it to the output stream
                             * of the socket. This data will be retrieved by the server device.
                             */
                            OutputStream outputStream = socket.getOutputStream();
                            ContentResolver cr = context.getContentResolver();
                            InputStream inputStream = cr.openInputStream(Uri.parse(Environment.getExternalStorageDirectory() + "/Android/data/edu.ntust.transferability/files/temp.tmp"));
                            while ((len = inputStream.read(buf)) != -1)
                            {
                                outputStream.write(buf, 0, len);
                            }
                            outputStream.close();
                            inputStream.close();
                        }
                        catch (FileNotFoundException e)
                        {
                            e.printStackTrace();
                        }
                        catch (IOException e)
                        {
                            e.printStackTrace();
                        }

                    }
                });
                thread.start();
            }
        }
    };

    WifiP2pManager.PeerListListener peerListListener = new WifiP2pManager.PeerListListener()
    {
        @Override
        public void onPeersAvailable(WifiP2pDeviceList wifiP2pDeviceList)
        {
            peers.clear();
            peers.addAll(wifiP2pDeviceList.getDeviceList());
            Log.d(ViewAppsActivity.tag, "!!!" + String.valueOf(peers.size()));
            if (peers.size() == 0)
            {
                Log.d(ViewAppsActivity.tag, "No devices found");
                return;
            }
            listViewListener.updateListView();
            //obtain a peer from the WifiP2pDeviceList
            for (WifiP2pDevice device : peers)
            {
                // WifiP2pDevice device = peers.get(0);
                Log.d(ViewAppsActivity.tag, "!!!" + device.deviceName);
                //                WifiP2pConfig config = new WifiP2pConfig();
                //                config.deviceAddress = device.deviceAddress;
                //                mManager.connect(mChannel, config, new WifiP2pManager.ActionListener()
                //                {
                //                    @Override
                //                    public void onSuccess()
                //                    {
                //                        Toast.makeText(mActivity.getApplicationContext(), "成功連接", Toast.LENGTH_LONG).show();
                //                    }
                //
                //                    @Override
                //                    public void onFailure(int reason)
                //                    {
                //                        //failure logic
                //                    }
                //                });
            }

        }
    };
}
