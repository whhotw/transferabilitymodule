package edu.ntust.transferability;

import android.app.Activity;
import android.content.Context;
import android.content.IntentFilter;
import android.net.Uri;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.os.Bundle;
import android.net.wifi.p2p.WifiP2pManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.File;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;


/**
 * Created by whh on 6/9/14.
 */
public class WifiDirectActivity extends Activity implements WiFiDirectBroadcastReceiver.ListViewListener, AdapterView.OnItemClickListener
{

    WifiP2pManager mManager;
    WifiP2pManager.Channel mChannel;
    WiFiDirectBroadcastReceiver mReceiver;
    IntentFilter mIntentFilter;

    private ListView lv_discover_device;
    private ImageView img;

    private Timer timer = null;
    private long foo = 0;

    int from = 0;

    @Override
    public void updateListView()
    {
        Log.d(ViewAppsActivity.tag, "!!! when onPeersAvailable update listview");
        ((ArrayAdapter) lv_discover_device.getAdapter()).notifyDataSetChanged();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wifidirect);



        mManager = (WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE);
        mChannel = mManager.initialize(this, getMainLooper(), null);
        mReceiver = new WiFiDirectBroadcastReceiver(mManager, mChannel, this, this);

        mIntentFilter = new IntentFilter();
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);

        lv_discover_device = (ListView) findViewById(R.id.lv_discover_device);
        img = (ImageView) findViewById(R.id.img);

        ArrayAdapter<WifiP2pDevice> arrayAdapter = new ArrayAdapter<WifiP2pDevice>(this, android.R.layout.simple_list_item_1, mReceiver.peers);
        lv_discover_device.setAdapter(arrayAdapter);
        lv_discover_device.setOnItemClickListener(this);

        Bundle extras = getIntent().getExtras();
        if (extras != null)
        {
            from = extras.getInt("from");
            if (from == 0){
                //lv_discover_device.setSelected(false);
            }
        }
    }

    /* register the broadcast receiver with the intent values to be matched */
    @Override
    protected void onResume()
    {
        super.onResume();
        registerReceiver(mReceiver, mIntentFilter);
    }

    /* unregister the broadcast receiver */
    @Override
    protected void onPause()
    {
        super.onPause();
        unregisterReceiver(mReceiver);
    }

    public void onClickToDiscover(View v)
    {
        if (timer == null)
        {
            timer = new Timer();
            timer.scheduleAtFixedRate(new TimerTask()
            {
                @Override
                public void run()
                {
                    runOnUiThread(new Runnable()
                    {

                        @Override
                        public void run()
                        {
                            if (foo % 2 == 0)
                            {
                                img.setImageResource(R.drawable.wifi2);
                            }
                            else
                            {
                                img.setImageResource(R.drawable.wifi);
                            }
                            foo++;
                        }
                    });
                }
            }, 0, 500);
        }

        mManager.discoverPeers(mChannel, new WifiP2pManager.ActionListener()
        {
            @Override
            public void onSuccess()
            {
                Log.d(ViewAppsActivity.tag, "!!! wifi p2p success discover peers");
            }

            @Override
            public void onFailure(int reasonCode)
            {
                Log.d(ViewAppsActivity.tag, "!!! wifi p2p failure discover peers");
            }
        });
    }


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
    {
        if (from == 1)
        {
            return;
        }
        Toast.makeText(view.getContext(), String.format("連接第%d個裝置", i + 1), Toast.LENGTH_LONG).show();

        WifiP2pDevice device = mReceiver.peers.get(i);

        WifiP2pConfig config = new WifiP2pConfig();
        config.deviceAddress = device.deviceAddress;

        mManager.connect(mChannel, config, new WifiP2pManager.ActionListener()
        {
            @Override
            public void onSuccess()
            {
                Toast.makeText(WifiDirectActivity.this.getApplicationContext(), "要求連接", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(int reason)
            {
                Toast.makeText(WifiDirectActivity.this.getApplicationContext(), "重複要求連接", Toast.LENGTH_SHORT).show();
                mManager.stopPeerDiscovery(mChannel, new WifiP2pManager.ActionListener()
                {
                    @Override
                    public void onSuccess()
                    {
                        img.setImageResource(R.drawable.wifi3);
                        if (timer != null)
                        {
                            timer.cancel();
                            timer = null;
                        }
                    }

                    @Override
                    public void onFailure(int i)
                    {
                        Toast.makeText(WifiDirectActivity.this.getApplicationContext(), "Stop service failed", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }
}
