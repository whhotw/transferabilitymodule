package edu.ntust.transferability.task;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;

import edu.ntust.transferability.ViewAppsActivity;

/**
 * Created by whh on 7/25/14.
 */
public class FileServerAsyncTask extends AsyncTask
{

    public  FileServerAsyncTask()
    {
        super();
    }

    @Override
    protected Void doInBackground(Object[] prarms)
    {
        try
        {

            /**
             * Create a server socket and wait for client connections. This
             * call blocks until a connection is accepted from a client
             */
            ServerSocket serverSocket = new ServerSocket(12345);
            Socket client = serverSocket.accept();

            /**
             * If this code is reached, a client has connected and transferred data
             * Save the input stream from the client as a file
             */
            final File f = new File(Environment.getExternalStorageDirectory() + "/Android/data/edu.ntust.transferability/files/temp.tmp");

            File dirs = new File(f.getParent());
            if (!dirs.exists())
            {
                dirs.mkdirs();
            }
            f.createNewFile();
            InputStream inputstream = client.getInputStream();
            //copyFile(inputstream, new FileOutputStream(f));
            serverSocket.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        return null;
    }

}
