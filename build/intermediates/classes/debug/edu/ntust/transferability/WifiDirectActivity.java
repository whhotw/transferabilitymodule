package edu.ntust.transferability;

import android.app.Activity;
import android.content.Context;
import android.content.IntentFilter;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.os.Bundle;
import android.net.wifi.p2p.WifiP2pManager;
import android.util.Log;
import android.view.View;

import org.json.JSONObject;

import java.util.Map;


/**
 * Created by whh on 6/9/14.
 */
public class WifiDirectActivity extends Activity
{

    WifiP2pManager mManager;
    WifiP2pManager.Channel mChannel;
    WiFiDirectBroadcastReceiver mReceiver;
    IntentFilter mIntentFilter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wifidirect);

        mManager = (WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE);
        mChannel = mManager.initialize(this, getMainLooper(), null);
        mReceiver = new WiFiDirectBroadcastReceiver(mManager, mChannel, this);

        mIntentFilter = new IntentFilter();
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);

    }

    /* register the broadcast receiver with the intent values to be matched */
    @Override
    protected void onResume()
    {
        super.onResume();
        registerReceiver(mReceiver, mIntentFilter);
    }

    /* unregister the broadcast receiver */
    @Override
    protected void onPause()
    {
        super.onPause();
        unregisterReceiver(mReceiver);
    }

    public void onClickToDiscover(View v)
    {
    	mManager.discoverPeers(mChannel, new WifiP2pManager.ActionListener() {
    	    @Override
    	    public void onSuccess() {
    	        Log.d(ViewAppsActivity.tag, "!!! wifi p2p success");
    	    }

    	    @Override
    	    public void onFailure(int reasonCode) {
    	    	Log.d(ViewAppsActivity.tag, "!!! wifi p2p failure");
    	    }
    	});
    }
    
}
