package edu.ntust.transferability;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pManager;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by whh on 6/9/14.
 */
public class WiFiDirectBroadcastReceiver extends BroadcastReceiver
{

    private WifiP2pManager mManager;
    private WifiP2pManager.Channel mChannel;
    private Activity mActivity;

    private List<WifiP2pDevice> peers = new ArrayList();

    public WiFiDirectBroadcastReceiver(WifiP2pManager manager, WifiP2pManager.Channel channel, Activity activity)
    {
        super();
        this.mManager = manager;
        this.mChannel = channel;
        this.mActivity = activity;
    }

    @Override
    public void onReceive(Context context, Intent intent)
    {
        String action = intent.getAction();

        if (WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION.equals(action))
        {
            // Check to see if Wi-Fi is enabled and notify appropriate activity
            int state = intent.getIntExtra(WifiP2pManager.EXTRA_WIFI_STATE, -1);
            if (state == WifiP2pManager.WIFI_P2P_STATE_ENABLED)
            {
                // Wifi P2P is enabled
                Log.d(ViewAppsActivity.tag, "!!! p2p enabled and support");
            }
            else
            {
                // Wi-Fi P2P is not enabled
                Log.d(ViewAppsActivity.tag, "!!! p2p not enabled and unsupported");
            }

        }
        else if (WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION.equals(action))
        {
            // Call WifiP2pManager.requestPeers() to get a list of current peers

            if (mManager != null)
            {
                mManager.requestPeers(mChannel, peerListListener);
            }

        }
        else if (WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION.equals(action))
        {
            // Respond to new connection or disconnections
        }
        else if (WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION.equals(action))
        {
            // Respond to this device's wifi state changing
        }
    }

    WifiP2pManager.PeerListListener peerListListener = new WifiP2pManager.PeerListListener()
    {
        @Override
        public void onPeersAvailable(WifiP2pDeviceList wifiP2pDeviceList)
        {
            peers.clear();
            peers.addAll(wifiP2pDeviceList.getDeviceList());

            if (peers.size() == 0)
            {
                Log.d(ViewAppsActivity.tag, "No devices found");
                return;
            }

            //obtain a peer from the WifiP2pDeviceList
            WifiP2pDevice device = peers.get(0);
            Log.d(ViewAppsActivity.tag, "!!!" + device.deviceName);
            WifiP2pConfig config = new WifiP2pConfig();
            config.deviceAddress = device.deviceAddress;

            mManager.connect(mChannel, config, new WifiP2pManager.ActionListener()
            {
                @Override
                public void onSuccess()
                {
                    Toast.makeText(mActivity.getApplicationContext(), "成功連接", Toast.LENGTH_LONG).show();
                }

                @Override
                public void onFailure(int reason)
                {
                    //failure logic
                }
            });

        }
    };
}
